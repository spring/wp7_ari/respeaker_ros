#
#
# Authors : N. Turro
#

import sys, getopt
import numpy as np
import future
import wave
import struct
from ros import rosbag
from respeaker_ros.msg import RawAudioData

def extract_audio(bag_path, topic_name, audio_path):
    print("Opening bag")
    bag = rosbag.Bag(bag_path)
    audio_file = wave.open(audio_path, 'w')
    
    print("Reading audio messages and saving to audio file")
    msg_count = 0
    
    for topic, msg, stamp in bag.read_messages(topics=[topic_name]):
        if msg._type == 'audio_common_msgs/AudioData': 
            ## Init
            if msg_count == 0:
                audio_file.setnchannels(1)
                audio_file.setframerate(16000)
                audio_file.setsampwidth(2)
            audio_file.writeframes(''.join(msg.data))
 
        if msg._type == 'respeaker_ros/RawAudioData':
            if msg_count == 0:
                #For debug only 
                #print(msg.data)
                audio_file.setnchannels(msg.nb_channel)
                audio_file.setframerate(msg.rate)
                audio_file.setsampwidth(msg.sample_byte_size)
                #For debug only
                #print("#channels = {} - rate = {} sample byte size = {} #frames = {}"
                #.format(msg.nb_channel, msg.rate, msg.sample_byte_size, msg.nb_frames))
            
            # read https://zach.se/generate-audio-with-python/
            # todo : pack format 'h' should be chosen according to msg.sample_byte_size (h corresponds to 2 bytes)
            new_frames = ''.join(''.join(struct.pack('h', sample) for sample in msg.data))
                           
            audio_file.writeframes(new_frames)
        msg_count += 1

    bag.close()
    audio_file.close()
    print("Done. {} audio messages written to {}".format(msg_count, audio_path))

# Input arguments handling and call to extract audio wav
def main(argv_list):
    
    print(" MAIN  {}".format(argv_list))
    input_file = ''
    topic_name = '/audio'
    output_file = 'audiofile.wav'
    
    try:
        opts, args = getopt.getopt(argv_list[1:],"hi:o:t:",["help", "ifile=","ofile=", "topic="])
    except getopt.GetoptError:
        print("{} -i <inputfile> -o <outputfile> -t <topic>".format(argv_list[0]))
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h","--help"):
            print("{} -i <inputfile> -o <outputfile> -t <topic>".format(argv_list[0]))
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_file = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg
        elif opt in ("-t", "--topic"):
            topic_name = arg
    
    print("Input file is {}".format(input_file))
    print("Output file is {}".format(output_file))
    extract_audio(input_file, topic_name, output_file)


# Main
if __name__ == "__main__":
   main(sys.argv[:])
