respeaker_ros
=============

A ROS Package for Respeaker Mic Array

## SPRING Fork

In order to use the SPRING audio topic, follow this procedure:

- Clone this repository to your ARI workspace (docker, ISO) and build 

```
cd ~/spring_ws/src
git clone https://gitlab.inria.fr/spring/wp7_ari/respeaker_ros.git
catkin build respeaker_ros
```

- Connect to ARI and deploy the package 

```
export ROS_MASTER_URI=http://ari-Xc:11311
rosrun pal_deploy deploy.py ari-Xc --package "respeaker_ros"
```

- Reboot ARI completely. You can also restart the "respeaker_ros" startup

```
ssh pal@ari-X
pal-stop respeaker_ros
pal-start respeaker_ros
```

- Verify it publishes the /audio/raw_audio data by default. 

```
pal@ari-9c:~$ rostopic hz /audio/raw_audio
subscribed to [/audio/raw_audio]
average rate: 30.889
	min: 0.000s max: 0.048s std dev: 0.01182s window: 29
```

- In order to record the audio, you can use rosbags. Say something as it is recording the bag.

```
ssh pal@ari-X
rosbag record -O audiotest.bag /audio/raw_audio
```

-You can then pass it to your laptop using scp. On your development laptop:

```
scp pal@ari-X:/home/pal/audiotest.bag your_directory
```

- Verify format of the bag either inside ARI or on your PC. 

```
root@c1697ea4db53:/spring/src/av_synchro# rosbag info inriaaudio.bag 
path:        inriaaudio.bag
version:     2.0
duration:    21.4s
start:       Jul 29 2021 16:24:39.56 (1627568679.56)
end:         Jul 29 2021 16:25:00.92 (1627568700.92)
size:        4.0 MB
messages:    668
compression: none [6/6 chunks]
types:       respeaker_ros/RawAudioData [1c397debf40a72ecc26b4cfd85d2a668]
topics:      /audio/raw_audio   668 msgs    : respeaker_ros/RawAudioData
```


-To verify correct recording you can convert the bag to a wav file using https://gitlab.inria.fr/spring/wp3_av_perception/av_synchro/-/blob/master/bag2wav.py.  Specifically, on your development computer: 


```
python bag2wav.py -i audiotest.bag -o audiotest.wav -t /audio/raw_audio
```

Note you may need to install the following if you are using ARI docker:

```
sudo apt-get install python-future
```

- !! the /audio/raw_audio has a message of type respeaker_ros/RawAudioData. Make sure the bag2wav.py has the corresponding msgs in L84:

```
if msg._type == 'respeaker_ros/RawAudioData':
```

## Supported Devices

- [Respeaker Mic Array v2.0](http://wiki.seeedstudio.com/ReSpeaker_Mic_Array_v2.0/)

    ![Respeaker Mic Array v2.0](https://github.com/SeeedDocument/ReSpeaker_Mic_Array_V2/raw/master/img/Hardware%20Overview.png)

## Preparation

1. Install this package

    ```bash
    mkdir -p ~/catkin_ws/src && ~/catkin_ws/src
    git clone https://github.com/furushchev/respeaker_ros.git
    cd ~/catkin_ws
    source /opt/ros/kinetic/setup.bash
    rosdep install --from-paths src -i -r -n -y
    catkin config --init
    catkin build respeaker_ros
    source ~/catkin_ws/devel/setup.bash
    ```

1. Register respeaker udev rules

    Normally, we cannot access USB device without permission from user space.
    Using `udev`, we can give the right permission on only respeaker device automatically.

    Please run the command as followings to install setting file:

    ```bash
    roscd respeaker_ros
    sudo cp -f $(rospack find respeaker_ros)/config/60-respeaker.rules /etc/udev/rules.d/60-respeaker.rules
    sudo systemctl restart udev
    ```

    And then re-connect the device.

1. Install python requirements

    ```bash
    roscd respeaker_ros
    sudo pip install -r requirements.txt
    ```

1. Update firmware

    ```bash
    git clone https://github.com/respeaker/usb_4_mic_array.git
    cd usb_4_mic_array
    sudo python dfu.py --download 6_channels_firmware.bin  # The 6 channels version 
    ```

## How to use

1. Run executables

    ```bash
    roslaunch respeaker_ros respeaker.launch
    rostopic echo /sound_direction     # Result of DoA
    rostopic echo /sound_localization  # Result of DoA as Pose
    rostopic echo /is_speeching        # Result of VAD
    rostopic echo /audio               # Raw audio
    rostopic echo /speech_audio        # Audio data while speeching
    ```

    You can also set various parameters via `dynamic_reconfigure`.

    ```bash
    sudo apt install ros-kinetic-rqt-reconfigure  # Install if not
    rosrun rqt_reconfigure rqt_reconfigure
    ```

    To set LED color, publish desired color:

    ```bash
    rostopic pub /status_led std_msgs/ColorRGBA "r: 0.0
    g: 0.0
    b: 1.0
    a: 0.3"
    ```

## Use cases

### Voice Recognition

- [ros_speech_recognition](https://github.com/jsk-ros-pkg/jsk_3rdparty/tree/master/ros_speech_recognition)
- [julius_ros](http://wiki.ros.org/julius_ros)

## Notes

The configuration file for `dynamic_reconfigure` in this package is created automatically by reading the parameters from devices.
Though it will be rare case, the configuration file can be updated as followings:

1. Connect the device to the computer.
1. Run the generator script.

    ```bash
    rosrun  respeaker_ros respeaker_gencfg.py
    ```
1. You will see the updated configuration file at `$(rospack find respeaker_ros)/cfg/Respeaker.cfg`.


## Author

Yuki Furuta <<furushchev@jsk.imi.i.u-tokyo.ac.jp>>

## License

[Apache License](LICENSE)
