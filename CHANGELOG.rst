^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package respeaker_ros
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.3 (2021-01-27)
------------------
* Merge branch 'add-diagnostics' into 'master'
  Add simple diagnostics to avoid start race condition with ASR
  See merge request ros-overlays/respeaker_ros!3
* Add simple diagnostics to avoid start race condition with ASR
* Contributors: Victor Lopez, victor

0.0.2 (2019-11-15)
------------------
* Merge branch 'animate_leds' into 'master'
  added a parameter to choose to animate LEDs or not
  See merge request ros-overlays/respeaker_ros!2
* addd 48k sample rate related firmware
* added a parameter to choose to animate LEDs or not
* Contributors: Sai Kishor Kothakota, Victor Lopez

0.0.1 (2019-10-17)
------------------
* Merge branch 'respeaker_fixes' into 'master'
  Respeaker fixes
  See merge request ros-overlays/respeaker_ros!1
* better handling of run_speech_recognition argument in roslaunch
* added some new dependencies to the package
* more build fixes
* remove wrong USE_SOURCE_PERMISSIONS for FILES install rule
* Added a copy of respeaker firmware to the package
* return from timer call when respeaker object is None
* added install rules for cfg and udev rules
* removed static transform and changes for using the device information
* Added parameter to publish device information alone
* fixed the speech-to-text confidence
* Merge pull request #1 from furushchev/master
  Sync with original repo
* Merge pull request #12 from furushchev/update
  Update to use catkin_virtualenv
* support catkin_vitrualenv
* support catkin_virtualenv
* add speech_to_text
* update
* Add scripts for audio recording and playback
* suppress alsa error
* Merge pull request #11 from furushchev/pub-diff
  Publish DOA,VAD only on state change
* Publish only updated
* Merge pull request #10 from furushchev/fix-typo
  fix typo in readme
* fix typo in readme
* minor bugfix
* Merge pull request #6 from furushchev/speech-audio
  add speech_audio
* add speech_audio
* add option: doa offset
* Merge pull request #5 from furushchev/udev
  Add udev rule
* add udev rule
* Fix typo
* add led functions
* Merge branch 'master' of github.com:furushchev/respeaker_ros
* fix: IOError on restart
* Create LICENSE
* track license
* update
* add python requirements
* Update README.md
* initial commit
* Contributors: Furushchev, Luca Marchionni, Sai Kishor Kothakota, Victor Lopez, Yuki Furuta
